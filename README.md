# fioul prices

[![License: WTFPL](https://img.shields.io/badge/License-WTFPL-brightgreen.svg)](http://www.wtfpl.net/about/)
[![pipeline status](https://gitlab.com/leucos/fioulprices/badges/master/pipeline.svg)](https://gitlab.com/leucos/fioulprices/-/commits/master)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/leucos/fioulprices)](https://goreportcard.com/report/gitlab.com/leucos/fioulprices)

Grabs fioul prices for France and stores them in influxdb.

## Quick start

In influxdb:

```sql
create database fioul WITH DURATION 10000d REPLICATION 1
```

```bash
fioulprices -influxserver http://192.168.1.254:8086 -influxdb database -influxuser user -influxpass password -influxmeasurement fioul -catchup
```

## Usage

```
./fioulprices -h
Usage of ./fioulprices:
  -catchup=false: catchup historical data if data source permits
  -influxdb="": influxdb database (no events are send if not set)
  -influxmeasurement="": influxdb measurement (default: none, required when server is set)
  -influxpass="": influxdb password (default: none)
  -influxserver="": influxdb server URL (no events are send if not set)
  -influxtimeout=2000: how many milliseconds do we allow influxdb POST to take (default: 2000)
  -influxuser="": influxdb username (default: none)
  -logformat="text": log format
  -loglevel="info": debug level
```

## Compile

```bash
go build .
```

## Graphing

See example dashboard in [fioul.json](fioul.json). Note that it requires
[trend box](https://grafana.com/grafana/plugins/btplc-trend-box-panel) panel.

## Licence

WTFPL