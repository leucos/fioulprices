package datefr

import (
	"fmt"
	"regexp"
	"strings"
	"time"
)

var monthMap = map[string]string{
	"janvier":   "01",
	"février":   "02",
	"mars":      "03",
	"avril":     "04",
	"mai":       "05",
	"juin":      "06",
	"juillet":   "07",
	"août":      "08",
	"septembre": "09",
	"octobre":   "10",
	"novembre":  "11",
	"décembre":  "12",
}

// Parse FR style date
func Parse(dteStr string) (time.Time, error) {
	dteStr = strings.TrimSpace(dteStr)
	space := regexp.MustCompile(`\s+`)
	dteStr = space.ReplaceAllString(dteStr, " ")

	s := strings.Split(dteStr, " ")
	day, monthname, year := s[1], s[2], s[3]

	for k, v := range monthMap {
		if monthname == k {
			monthname = v
			break
		}
	}

	date := fmt.Sprintf("%s %s %s", day, monthname, year)
	t, err := time.Parse("2 01 2006", date)

	if err != nil {
		return time.Time{}, fmt.Errorf("unable to parse date %s: %v", date, err)
	}

	return t, nil
}
