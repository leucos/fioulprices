module gitlab.com/leucos/fioulprices

go 1.14

require (
	github.com/PuerkitoBio/goquery v1.5.1
	github.com/influxdata/influxdb1-client v0.0.0-20191209144304-8bf82d3c094d
	github.com/namsral/flag v1.7.4-pre
	github.com/sirupsen/logrus v1.5.0
	github.com/stretchr/testify v1.3.0 // indirect
)
