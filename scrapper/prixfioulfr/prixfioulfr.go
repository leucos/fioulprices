package prixfioulfr

import (
	"bufio"
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
	"gitlab.com/leucos/fioulprices/datefr"

	log "github.com/sirupsen/logrus"
)

const sourceUrl = "https://prixfioul.fr/"

// PrixFioulFR grabs data from https://prixfioul.fr/
type PrixFioulFR struct {
}

func reverse(a [][]string) [][]string {
	for i := len(a)/2 - 1; i >= 0; i-- {
		opp := len(a) - 1 - i
		a[i], a[opp] = a[opp], a[i]
	}
	return a
}

// Name for the source
func (p PrixFioulFR) Name() string {
	return "prixfioul"
}

// CatchUp with historical data
func (p PrixFioulFR) CatchUp() (map[time.Time]float64, error) {
	// Request the HTML page.
	log.Infof("scrapping historical data for %s", sourceUrl)

	res, err := http.Get(sourceUrl + "pf/maj/data0.js")
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()
	if res.StatusCode != 200 {
		return nil, fmt.Errorf("status code error: %d %s", res.StatusCode, res.Status)
	}

	values := [][]string{}

	scanner := bufio.NewScanner(res.Body)
	for scanner.Scan() {
		text := scanner.Text()
		if text == "values=[" || text == "];" {
			continue
		}

		for _, c := range []string{"[", "]", "'"} {
			text = strings.ReplaceAll(text, c, "")
		}
		text = strings.Trim(text, ",")

		splitted := strings.Split(text, ",")
		values = append(values, []string{splitted[0], splitted[1]})
	}
	if err := scanner.Err(); err != nil {
		return nil, fmt.Errorf("reading archive data for %s: %v", sourceUrl, err)
	}

	values = reverse(values)

	prices := make(map[time.Time]float64)
	year := time.Now().Year()
	prevmonth := strings.Split(values[0][0], "/")[0]

	for _, v := range values {
		curmonth := strings.Split(v[0], "/")[0]
		if curmonth == "12" && prevmonth != "12" {
			year -= 1
		}
		prevmonth = curmonth

		date, err := time.Parse("02/01/2006", v[0]+"/"+strconv.Itoa(year))
		if err != nil {
			return nil, fmt.Errorf("unable to parse date %s: %v", v[0]+"/"+strconv.Itoa(year), err)
		}

		pf, err := strconv.ParseFloat(v[1], 64)
		if err != nil {
			return nil, fmt.Errorf("unable to parse price %s: %v", v[1], err)
		}

		prices[date] = pf
	}

	newprices, err := p.Scrape()
	if err != nil {
		return nil, err
	}

	for k, v := range newprices {
		prices[k] = v
	}

	return prices, nil
}

// Scrape current or recent data
func (p PrixFioulFR) Scrape() (map[time.Time]float64, error) {
	prices := make(map[time.Time]float64)

	log.Infof("scrapping %s", sourceUrl)

	// Request the HTML page.
	res, err := http.Get(sourceUrl)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()
	if res.StatusCode != 200 {
		return nil, fmt.Errorf("status code error: %d %s", res.StatusCode, res.Status)
	}

	// Load the HTML document
	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		log.Fatal(err)
	}

	log.Debugf("got data from %s, checking prices", sourceUrl)

	// Find the review items
	doc.Find("#contenu > div.item-page > div > table:nth-child(8) > tbody > tr").
		Not("#contenu > div.item-page > div > table:nth-child(8) > tbody > tr.ligne_entete").
		Each(func(i int, s *goquery.Selection) {
			dte := s.Find("th").Text()
			date, err := datefr.Parse(dte)
			if err != nil {
				log.Errorf("unable to parse date '%s': %v", dte, err)
				return
			}
			price := s.Find("td").First().Text()
			// diff := s.Find("td").Last().Text()
			// log.Infof("%d: %s - %s (%s)\n", i, date, price, diff)
			price = strings.Trim(price, "€")

			pf, err := strconv.ParseFloat(price, 64)
			if err != nil {
				log.Errorf("unable to parse price %s: %v", price, err)
				return
			}
			log.Debugf("found price %f at %s", pf, date)
			prices[date] = pf
		})

	return prices, nil
}
