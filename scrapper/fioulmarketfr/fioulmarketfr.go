package fioulmarketfr

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
	"gitlab.com/leucos/fioulprices/datefr"

	log "github.com/sirupsen/logrus"
)

const sourceUrl = "https://www.fioulmarket.fr/"

// FioulMarketFR handles data from https://www.fioulmarket.fr/prix-fioul
type FioulMarketFR struct {
}

// Name for the source
func (p FioulMarketFR) Name() string {
	return "fioulmarket"
}

// CatchUp with historical data
func (p FioulMarketFR) CatchUp() (map[time.Time]float64, error) {
	type History struct {
		Dates           []string `json:"dates"`
		ReferencePrices []struct {
			X         float64 `json:"x"`
			DateStart string  `json:"dateStart"`
			DateEnd   string  `json:"dateEnd"`
			Y         float64 `json:"y"`
		} `json:"referencePrices"`
		FioulmarketPrices []struct {
			X         float64 `json:"x"`
			DateStart string  `json:"dateStart"`
			DateEnd   string  `json:"dateEnd"`
			Y         float64 `json:"y"`
		} `json:"fioulmarketPrices"`
	}

	log.Infof("scrapping historical data for %s", sourceUrl)

	res, err := http.Get(sourceUrl + "chart-price/country/1")
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()
	if res.StatusCode != 200 {
		return nil, fmt.Errorf("status code error: %d %s", res.StatusCode, res.Status)
	}

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, fmt.Errorf("unable rto read body: %v", err)

	}

	var hist History
	err = json.Unmarshal(body, &hist)
	if err != nil {
		return nil, fmt.Errorf("unable tu unmarshal body: %v", err)
	}

	prices := make(map[time.Time]float64)

	for _, ref := range hist.FioulmarketPrices {
		start, err := time.Parse("2006-01-02", ref.DateStart)
		if err != nil {
			return nil, fmt.Errorf("unable to parse start date %s: %v", ref.DateStart, err)
		}
		end, err := time.Parse("2006-01-02", ref.DateEnd)
		if err != nil {
			return nil, fmt.Errorf("unable to parse end date %s: %v", ref.DateEnd, err)
		}
		prices[start] = float64(ref.Y)
		prices[end] = float64(ref.Y)
	}

	newprices, err := p.Scrape()
	if err != nil {
		return nil, err
	}

	for k, v := range newprices {
		prices[k] = v
	}

	return prices, nil
}

// Scrape current or recent data
func (p FioulMarketFR) Scrape() (map[time.Time]float64, error) {
	prices := make(map[time.Time]float64)

	log.Infof("scrapping %s", sourceUrl+"prix-fioul")

	// Request the HTML page.
	res, err := http.Get(sourceUrl + "prix-fioul")
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()
	if res.StatusCode != 200 {
		return nil, fmt.Errorf("status code error: %d %s", res.StatusCode, res.Status)
	}

	// Load the HTML document
	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		log.Fatal(err)
	}

	log.Debugf("got data from %s, checking prices", sourceUrl)

	// Find the review items
	doc.Find("#main > section > article.post.evolution-table > div > div > div.col-lg-8 > div > table > tbody > tr").
		Each(func(i int, s *goquery.Selection) {
			dte := s.Find("td").First().Text()
			date, err := datefr.Parse(dte)
			if err != nil {
				log.Errorf("unable to parse date '%s': %v", dte, err)
				return
			}
			price := s.Find("td").Next().First().Text()
			// diff := s.Find("td").Last().Text()
			log.Debugf("%d: %s - %s\n", i, date, price)
			price = strings.Trim(price, "€")

			pf, err := strconv.ParseFloat(price, 64)
			if err != nil {
				log.Errorf("unable to parse price %s: %v", price, err)
				return
			}
			log.Debugf("found price %f at %s", pf, date)
			prices[date] = pf
		})

	return prices, nil
}
