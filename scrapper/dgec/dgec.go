package dgec

import (
	"fmt"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"

	log "github.com/sirupsen/logrus"
)

const sourceUrl = "https://www.prix-carburants.developpement-durable.gouv.fr/petrole/se_resul_fr.php"

// DGEC handles data from https://www.prix-carburants.developpement-durable.gouv.fr/petrole/se_cons_fr.htm
type DGEC struct {
}

func newSlice(start, end int) []int {
	s := make([]int, end-start+1)
	for i := range s {
		s[i] = start + i
	}
	return s
}

// Name for the source
func (p DGEC) Name() string {
	return "dgec"
}

// CatchUp with historical data
func (p DGEC) CatchUp() (map[time.Time]float64, error) {
	prices := make(map[time.Time]float64)

	for _, year := range newSlice(1985, time.Now().Year()) {
		y, err := scrapeYear(year)
		if err != nil {
			log.Errorf("unable to scrap year %d: %v", year, err)
			continue
		}
		for k, v := range y {
			prices[k] = v
		}
	}
	return prices, nil
}

// Scrape current or recent data
func (p DGEC) Scrape() (map[time.Time]float64, error) {
	prices, err := scrapeYear(time.Now().Year())
	if err != nil {
		log.Errorf("unable to scrap current year: %v", err)
	}
	return prices, nil
}

func scrapeYear(y int) (map[time.Time]float64, error) {
	prices := make(map[time.Time]float64)
	log.Infof("scrapping %s for year %d", sourceUrl, y)

	// Request data
	// We only request data for this year
	// date1 := time.Date(time.Now().Year(), time.January, 1, 0,0,0,time.UTC)
	// date1 := fmt.Sprintf("01/01/%d", time.Now().Year())
	res, err := http.PostForm(sourceUrl, url.Values{
		"date_1":    {fmt.Sprintf("01/01/%d", y)},
		"date_2":    {fmt.Sprintf("31/12/%d", y)},
		"categorie": {"2"},
		"_Action":   {"40"},
		"monnaie":   {"E"},
	})

	if err != nil {
		return nil, err
	}

	defer res.Body.Close()
	if res.StatusCode != 200 {
		return nil, fmt.Errorf("status code error: %d %s", res.StatusCode, res.Status)
	}

	// Load the HTML document
	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		log.Fatal(err)
	}

	log.Debugf("got data from %s, checking prices", sourceUrl)

	// Find the review items
	doc.Find("#formu > table > tbody > tr").Slice(4, goquery.ToEnd).
		Each(func(i int, s *goquery.Selection) {
			dte := s.Find("td").First().Text()
			date, err := time.Parse("02/01/2006", dte)
			if err != nil {
				log.Errorf("unable to parse date '%s': %v", dte, err)
				return
			}

			var col int
			switch {
			case y < 1987:
				col = 2
			case y >= 1987 && y < 1992:
				col = 3
			case y >= 1992 && y < 2010:
				col = 6
			case y >= 2010:
				col = 4
			}

			price := s.Find("td").Eq(col).First().Text()
			log.Debugf("%d: %s - %s\n", i, date, price)
			price = strings.Replace(price, ",", ".", 1)
			pf, err := strconv.ParseFloat(price, 64)
			if err != nil {
				log.Errorf("unable to parse price %s: %v", price, err)
				return
			}

			// Convert per 1000L
			pf *= 1000
			log.Debugf("found price %f at %s", pf, date)
			prices[date] = pf
		})

	return prices, nil
}
