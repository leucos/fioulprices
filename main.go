package main

import (
	"fmt"
	"os"
	"sync"
	"time"

	"github.com/namsral/flag"

	log "github.com/sirupsen/logrus"

	"gitlab.com/leucos/fioulprices/influx"
	"gitlab.com/leucos/fioulprices/scrapper/dgec"
	"gitlab.com/leucos/fioulprices/scrapper/fioulmarketfr"
	"gitlab.com/leucos/fioulprices/scrapper/prixfioulfr"
)

// Scrapper defines what metrics handlers should implement
type Scrapper interface {
	Name() string
	Scrape() (map[time.Time]float64, error)
	CatchUp() (map[time.Time]float64, error)
}

func main() {
	errs := run(os.Args)
	if errs != nil {
		log.Errorf("got %d errors while scrapping sources", len(errs))
		for _, err := range errs {
			log.Errorf("unable to scrap prices: %v", err)
		}
		os.Exit(1)
	}
	log.Info("scrap done")
}

func run(args []string) []error {
	fs := flag.NewFlagSetWithEnvPrefix(args[0], "FIOUL", flag.ExitOnError)

	var influxServer = fs.String("influxserver", "", "influxdb server URL (no events are send if not set)")
	var influxDB = fs.String("influxdb", "", "influxdb database (no events are send if not set)")
	var influxUser = fs.String("influxuser", "", "influxdb username (default: none)")
	var influxPass = fs.String("influxpass", "", "influxdb password (default: none)")
	var influxMeasurement = fs.String("influxmeasurement", "", "influxdb measurement (default: none, required when server is set)")
	// var influxTags = fs.String("influxtags", "", "comma-separated k=v pairs of influxdb tags (default: none, example: 'foo=bar,fizz=buzz')")
	// var influxRetry = fs.Int("influxretry", 3, "how many times we try to send the event to influxdb (default: 3)")
	var influxTimeout = fs.Int("influxtimeout", 2000, "how many milliseconds do we allow influxdb POST to take (default: 2000)")
	var level = fs.String("loglevel", "info", "debug level")
	var format = fs.String("logformat", "text", "log format")
	var catchup = fs.Bool("catchup", false, "catchup historical data if data source permits")

	err := fs.Parse(os.Args[1:])
	if err != nil {
		return []error{fmt.Errorf("unable to parse arguments: %v", err)}
	}

	// Setup logging
	l, err := log.ParseLevel(*level)
	if err != nil {
		return []error{fmt.Errorf("unable to understand requested logging level %s: %v", *level, err)}
	}
	log.SetLevel(l)

	if *format == "json" {
		log.SetFormatter(&log.JSONFormatter{})
	}

	inf := &influx.Config{
		DB:          *influxDB,
		URL:         *influxServer,
		User:        *influxUser,
		Pass:        *influxPass,
		Measurement: *influxMeasurement,
		Timeout:     time.Duration(*influxTimeout) * time.Second,
	}

	// Create measurers
	scrappers := []Scrapper{
		prixfioulfr.PrixFioulFR{},
		fioulmarketfr.FioulMarketFR{},
		dgec.DGEC{},
	}

	inf.Init()

	return scrap(scrappers, inf, *catchup)
}

func scrap(s []Scrapper, inf *influx.Config, catchup bool) []error {
	var errs []error
	var err error
	var wg sync.WaitGroup
	var prices map[time.Time]float64

	if catchup {
		log.Infof("starting fioul price scrapper in catchup mode")
	} else {
		log.Infof("starting fioul price scrapper")
	}

	for _, scrapper := range s {
		if catchup {
			prices, err = scrapper.CatchUp()
		} else {
			prices, err = scrapper.Scrape()
		}
		if err != nil {
			errs = append(errs, err)
		}
		wg.Add(1)
		go inf.Log(influx.DataPoints{Source: scrapper.Name(), Points: prices}, &wg)
	}

	wg.Wait()
	return errs
}
