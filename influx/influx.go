package influx

import (
	"fmt"
	"math/rand"
	"sync"
	"time"

	influxv2 "github.com/influxdata/influxdb1-client/v2"
	log "github.com/sirupsen/logrus"
)

// Config information
type Config struct {
	DB          string
	URL         string
	User        string
	Pass        string
	Retries     int
	Timeout     time.Duration
	Measurement string
	Tags        string
}

// DataPoints to write
type DataPoints struct {
	Source string
	Points map[time.Time]float64
}

// Init is fake here since we need no init
func (c Config) Init() {
	if c.URL == "" || c.Measurement == "" {
		log.Info("influxdb not activated (influxurl or influxmeasurement not set)")
		return
	}

	log.Debugf("influxdb initialized for %s", c.URL)
}

// Log a point to a server
func (c *Config) Log(points DataPoints, wg *sync.WaitGroup) {
	defer wg.Done()

	// Null-Object type bypass if there is no server information
	if c.DB == "" || c.URL == "" {
		return
	}

	err := c.doLog(points)
	if err != nil {
		log.Errorf("unable to log points: %v", err)
	}
}

func (c *Config) getInfluxV2Config() influxv2.HTTPConfig {
	return influxv2.HTTPConfig{
		Addr:     c.URL,
		Username: c.User,

		Password: c.Pass,
		Timeout:  c.Timeout,
	}
}

func (c *Config) doLog(points DataPoints) error {
	conf := c.getInfluxV2Config()
	client, err := influxv2.NewHTTPClient(conf)
	if err != nil {
		return fmt.Errorf("error creating InfluxDB Client: %v", err.Error())
	}

	rand.Seed(42)

	bp, _ := influxv2.NewBatchPoints(influxv2.BatchPointsConfig{
		Database:  c.DB,
		Precision: "us",
	})

	tags := map[string]string{
		"source": points.Source,
	}

	for k, v := range points.Points {
		fields := map[string]interface{}{
			"price": v,
		}

		pt, err := influxv2.NewPoint(
			c.Measurement,
			tags,
			fields,
			k,
		)
		if err != nil {
			log.Warnf("unable to add new point: %v", err)
			continue
		}
		log.Debugf("adding influxdb point %s %f", k, v)
		bp.AddPoint(pt)
	}

	err = client.Write(bp)
	if err != nil {
		log.Errorf("unable to add poitns: %v", err)
		return err
	}

	return nil
}
